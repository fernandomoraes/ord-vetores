#include <stdio.h>
#include <stdlib.h>
#ifdef _WIN32
#include <locale.h>
#endif

void bubble(int vet[],int n){
        int aux, j, k;

        for(k = n - 1;k > 0;k--){
            //printf("\n[%d]", k);
            for(j = 0; j < k; j++){
                //printf("%d, ", j);
                if(vet[j] > vet[j + 1]){
                    aux = vet[j];
                    vet[j] = vet[j + 1];
                    vet[j + 1] = aux;
                }
        }
    }
}

int particao(int vet[], int inf, int sup){
    int a, down, up, aux;

    a = vet[inf];
    up = sup;
    down = inf;

    while(down < up){
        while(vet[down] <= a && down < sup){
            down++;
        }while(vet[up] > a){
                up--;
        }if(down < up){
            aux = vet[up];
            vet[up] = vet[down];
            vet[down] = aux;
    }
    }
    vet[inf] = vet[up];
    vet[up] = a;

    return up;

}

void quick(int vet[], int inf,int sup){
    int j;
    if(inf > sup) return;
    j = particao(vet, inf, sup);
    quick(vet, inf, j-1);
    quick(vet, j+1, sup);

}

void insert(int vet[], int n){
    int i, j, temp;

    for(i = n - 2;i>=0;i--){
        temp = vet[i];
        j = i + 1;
        while(j <= n - 1 && vet[j] < temp){
            vet[j - 1] = vet[j];
            j = j + 1;
        }
    vet[j - 1] = temp;
    }
   

}

void shell(int vet[], int n){
    int i, j, h, temp;
   
    for(h = 1; h < n; h = 3*h+1);

    while(h > 0){
        h = (h - 1) / 3;
        for(i = h;i < n; i++){
            temp = vet[i];
            j = i;
            while(vet[j - h] > temp){
                vet[j] = vet[j - h];
                j = j - h;
                if(j < h) break;
            }

            vet[j] = temp;
        }
}
}

void main(void){
    #ifdef _WIN32
    setlocale(LC_ALL, "Portuguese");
    #endif
    int n, i, opcao;

    printf("\n/***********************ORDENAÇÃO DE VETORES****************************/\n");

    printf("Digite a quantidade de elementos do vetor: ");
    scanf("%d", &n);

    int vet[n];

    for(i = 0;i<n;i++){
        printf("Digite um inteiro: ");
        scanf("%d", &vet[i]);
    }
    while(opcao != 5){
    printf("0 - Metodo Bolha\n1 - Metodo Rapido\n2 - Metodo de Inserção\n3 - Metodo Shell\n4 - Limpa Terminal\n5 - Sair\n");
    scanf("%d", &opcao);

    switch(opcao){
        case 0:
            bubble(vet, n);
            printf("/*****VETOR ORDENADO*****/\n");
            for(i = 0;i < n; i++){
                printf("%d\n", vet[i]);
            }
            continue;
        case 1:
            quick(vet, 0, n-1);
            printf("/*****VETOR ORDENADO*****/\n");
            for(i = 0;i < n; i++){
                printf("%d\n", vet[i]);
            }
            continue;
        case 2:
            insert(vet, n);
            printf("/*****VETOR ORDENADO*****/\n");
            for(i = 0;i < n; i++){
                 printf("%d\n", vet[i]);
            }
            break;
        case 3:
            shell(vet, n);
            printf("/*****VETOR ORDENADO*****/\n");
            for(i = 0;i < n; i++){
                printf("%d\n", vet[i]);
            }
            break;
        case 4:
            #ifdef linux
            system("clear");
            #endif
            #ifdef _WIN32
            system("cls");
            #endif
            break;
        default:
            break;
            exit(0);
    }
    continue;
    }
   

}